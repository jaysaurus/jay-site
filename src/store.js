export default function store (vue) {
  return {
    state () {
      return {
        calendar: [],
        eventBooking: {},
        eventIsVisible: false,
        isLoading: false,
        modalIsVisible: false,
        modifier: 'large',
        subjectDate: ''
      }
    },
    mutations: {
      SET_EVENT_BOOKING (state, o) {
        state.eventBooking = o
      },
      SET_CALENDAR (state, o) {
        state.calendar = o
      },
      SET_IS_LOADING (state, o) {
        state.isLoading = o
      },
      SET_SUBJECT_DATE (state, o) {
        state.subjectDate = new Date(o).toDateString()
      },
      SET_MODIFIER (state, o) {
        if (!o) state.subjectDate = ''
        state.modifier = o
      },
      SHOW_HIDE_EVENT (state, o) {
        state.eventIsVisible = o
      },
      SHOW_HIDE_MODAL (state, o) {
        state.modalIsVisible = o
      }
    },
    actions: {
      async setCalendar ({commit, state}, c) {
        try {
          const {data} =
            await vue.axios.get('http://www.jayedwardscode.com/calendar')
          commit('SET_CALENDAR', data)
        } catch (e) {
          commit('SET_CALENDAR', false)
          console.error(e.message)
        }
      }
    }
  }
}
