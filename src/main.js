// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import App from './App'
import router from './router'
import store from './store'

import Icon from 'vue-awesome/components/Icon'
import iconRegister from './iconRegister'
import 'purecss'

Vue.use(VueAxios, axios)
Vue.use(Vuex)
Icon.register(iconRegister)

Vue.config.productionTip = false

Vue.extend({
  methods: {
    firstLetterUpperCase (string) {
      return string.charAt(0).toUpperCase() + string.slice(1)
    }
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store: new Vuex.Store(store(Vue)),
  components: { App },
  template: '<App/>'
})
