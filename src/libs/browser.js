module.exports = {
  isChrome: (function () {
    const isChromium = window.chrome
    const winNav = window.navigator
    const vendorName = winNav.vendor
    const isOpera = typeof window.opr !== 'undefined'
    const isIEedge = winNav.userAgent.indexOf('Edge') > -1
    const isIOSChrome = winNav.userAgent.match('CriOS')
    return (isIOSChrome) || (
      isChromium !== null &&
      typeof isChromium !== 'undefined' &&
      vendorName === 'Google Inc.' &&
      isOpera === false &&
      isIEedge === false
    )
  })(),
  isIE: navigator.userAgent.match(/Trident.*rv:11\./) ||
    navigator.userAgent.indexOf('Edge') > -1 ||
    navigator.appVersion.indexOf('Edge') > -1 ||
    (/Edge/.test(navigator.userAgent)),
  isSafari: (function () {
    var ua = window.navigator.userAgent
    var iOS = !!ua.match(/iP(ad|od|hone)/i)
    var hasSafariInUa = !!ua.match(/Safari/i)
    var noOtherBrowsersInUa = !ua.match(/Chrome|CriOS|OPiOS|mercury|FxiOS|Firefox/i)
    var result = false
    if (iOS) { // detecting Safari in IOS mobile browsers
      var webkit = !!ua.match(/WebKit/i)
      result = webkit && hasSafariInUa && noOtherBrowsersInUa
    } else if (window.safari !== undefined) { // detecting Safari in Desktop Browsers
      result = true
    } else { // detecting Safari in other platforms
      result = hasSafariInUa && noOtherBrowsersInUa
    }
    return result
  })()
}
