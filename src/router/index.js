import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const imports = {
  'Index': () => import('@/components/uri/Index.vue'),
  'Miscellaneous': () => import('@/components/uri/Misc.vue'),
  'Sent': () => import('@/components/uri/Sent.vue'),
  'Skills': () => import('@/components/uri/Skills.vue'),
  'Smarts': () => import('@/components/uri/Smarts.vue'),
  'Work': () => import('@/components/uri/Work.vue'),
  'Where': () => import('@/components/uri/Where.vue'),
  '404': () => import('@/components/uri/404.vue')
}

export default new Router({
  mode: 'history',
  routes:
    Object.keys(imports)
      .reduce(
        (arr, key) => {
          arr.push({
            path: `/${(key !== 'Index') ? key.toLowerCase() : ''}`,
            name: key,
            component: imports[key]
          })
          return arr
        }, [{ path: '*', name: 'four-o-four', component: imports['404'] }])
})
