// derived from https://stackoverflow.com/questions/13016379/javascript-smooth-scroll-on-click#answer-51005536
export default function scrollToTop (duration) {
  let start = window.scrollY || document.documentElement.scrollTop
  let change = -start
  let currentTime = 0
  let increment = 5
  let debounce
  let animateScroll = function () {
    currentTime += increment
    let val = Math.easeInOutQuad(currentTime, debounce || start, change, duration)
    debounce = val
    document.body.scrollTop = document.documentElement.scrollTop = val
    if (currentTime < duration) {
      if ((debounce || start) > 0) {
        setTimeout(animateScroll, increment)
      }
    }
  }
  animateScroll()
}

Math.easeInOutQuad = function (t, b, c, d) {
  t /= d / 2
  if (t < 1) return c / 2 * t * t + b
  t--
  return -c / 2 * (t * (t - 2) - 1) + b
}
