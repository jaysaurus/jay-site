// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  "plugins": {
    "postcss-import": {},
    "postcss-url": {},
    "postcss-will-change-transition": {},
    // "postcss-custom-properties": {},
    // "postcss-css-variables": {},
    // "postcss-calc": {
    //   mediaQueries: true
    // },
    // to edit target browsers: use "browserslist" field in package.json
    "autoprefixer": {}
  }
}
