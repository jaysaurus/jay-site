#!/bin/bash

echo 'building'
npm run build

echo 'copying'
cp -R ./dist/* ../jay-edwards-code/public

echo 'job done!'
